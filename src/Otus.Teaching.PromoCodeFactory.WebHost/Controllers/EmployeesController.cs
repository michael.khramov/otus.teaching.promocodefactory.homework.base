﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Создать сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Create(CreateEmployeeRequest request)
        {
            if (request == null)
                return BadRequest();

            var employee = Fill(request);

            await _employeeRepository.CreateAsync(employee);

            return Ok();
        }

        /// <summary>
        /// Обновить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult> Update(UpdateEmployeeRequest request)
        {
            if (request == null)
                return BadRequest();

            if (request.Id == Guid.Empty)
                return BadRequest();

            var existingEmployee = await _employeeRepository.GetByIdAsync(request.Id);
            if (existingEmployee == null)
                return NotFound();

            var employee = Fill(request);
            employee.Roles = existingEmployee.Roles;
            employee.Id = existingEmployee.Id;

            await _employeeRepository.UpdateAsync(employee);

            return Ok();
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ActionResult> Delete(Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest();

            var employee = await _employeeRepository.GetByIdAsync(id);
            if (employee == null)
                return NotFound();
                        
            await _employeeRepository.DeleteAsync(employee.Id);

            return Ok();
        }

        /// <summary>
        /// Заполнить Employee данными
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private Employee Fill(CreateEmployeeRequest request)
        { 
            return new Employee()
            {
                AppliedPromocodesCount = request.AppliedPromocodesCount,
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Id = Guid.NewGuid(),
                Roles = new List<Role>(),
            };
        }
    }
}