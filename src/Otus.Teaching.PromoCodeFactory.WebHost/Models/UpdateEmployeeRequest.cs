﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    /// <summary>
    /// Update Employee Request
    /// </summary>
    public class UpdateEmployeeRequest: CreateEmployeeRequest
    {
        /// <summary>
        /// Id
        /// </summary>
        public Guid Id { get; set; }
      
    }
}