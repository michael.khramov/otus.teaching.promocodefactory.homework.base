﻿
namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    /// <summary>
    /// Create Employee Request
    /// </summary>
    public class CreateEmployeeRequest
    {
        /// <summary>
        /// First Name
        /// </summary>
        public string FirstName { get; set; }
        
        /// <summary>
        /// Last Name
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Applied Promocodes Count
        /// </summary>
        public int AppliedPromocodesCount { get; set; }
    }
}